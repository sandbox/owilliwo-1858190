<?php

/**
 * @file
 * Template for bulk update page.
 *
 * Available variables:
 * - $catchline: Catchline (display number of nodes to be processed
 * - $bulk_update_form: bulk update launch form
 *
 * @see template_preprocess()
 * @see template_preprocess_block()
 * @see template_process()
 *
 * @ingroup themeable
 */

?>
<p><?php print $catchline; ?></p>
<?php if ($bulk_update_form):  ?>
  <?php print $bulk_update_form; ?>
<?php endif; ?>
