<?php

/**
 * @file
 * Administration form functions.
 *
 * @author Philippe MOUCHEL.
 */

/**
 * Administration form settings.
 *
 * @return array
 *   containing form settings
 */
function _delayed_publication_administration_form() {
  $form = array();

  // Get concerned node types.
  $node_types = node_type_get_names();
  $field = field_info_field(DELAYED_PUBLICATION_REAL_PUBLISHED_FIELD_NAME);

  // Checkboxes to select node types.
  $form['delayed_publication_admin_delayable_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t("Activate delayed publication for the following node types:"),
    '#options' => $node_types,
    '#default_value' => variable_get('delayed_publication_node_types', array()),
    '#multiple' => TRUE,
  );

  // Submit button.
  $form['delayed_publication_submit'] = array(
    '#type' => 'submit',
    '#value' => t("Save configuration"),
  );

  // Create and display form.
  return $form;
}

/**
 * Administration form submission.
 *
 * @param array $form
 *   containing form settings
 * @param array $form_state
 *   containing form values
 */
function _delayed_publication_administration_form_submit($form, &$form_state) {

  // Set some field configuration.
  $fields = array(
    DELAYED_PUBLICATION_FIELD_NAME,
    DELAYED_PUBLICATION_REAL_PUBLISHED_FIELD_NAME,
  );
  $fields_labels = array(
    DELAYED_PUBLICATION_FIELD_NAME => t("Delayed publication"),
    DELAYED_PUBLICATION_REAL_PUBLISHED_FIELD_NAME => t("Real published date"),
  );
  $fields_requireds = array(
    DELAYED_PUBLICATION_FIELD_NAME => FALSE,
    DELAYED_PUBLICATION_REAL_PUBLISHED_FIELD_NAME => TRUE,
  );
  $fields_defaults = array(
    DELAYED_PUBLICATION_FIELD_NAME => 'blank',
    DELAYED_PUBLICATION_REAL_PUBLISHED_FIELD_NAME => 'now',
  );

  // Date popup verification.
  $date_popup_enabled = module_exists('date_popup');

  // Récupération du champ à instancier.
  $field_1 = field_info_field($fields[0]);
  $field_2 = field_info_field($fields[1]);

  // Field exist verification.
  if (!$field_1) {

    // Error message for first field.
    drupal_set_message(t("<em>@field_name</em> doesn't exist. Instances can't be created.", array('@field_name' => $fields[0])), 'error');
  }
  if (!$field_2) {

    // Error message for second field.
    drupal_set_message(t("<em>@field_name</em> doesn't exist. Instances can't be created.", array('@field_name' => $fields[1])), 'error');
  }

  // If both fields exist.
  else {

    // Foreach selected node types.
    $nodes_state = $form_state['values']['delayed_publication_admin_delayable_node_types'];
    foreach ($nodes_state as $machine_name => $delayable) {
      foreach ($fields as $field_name) {

        // Instance configuration (with ou without date_popup).
        $instance_config = array(
          'field_name' => $field_name,
          'entity_type' => 'node',
          'bundle' => $machine_name,
          'label' => $fields_labels[$field_name],
          'required' => $fields_requireds[$field_name],
          'widget' => array(
            'module' => 'date',
            'type' => $date_popup_enabled ? 'date_popup' : 'date_text',
            'settings' => array(
              'input_format' => 'd/m/Y - H:i:s',
              'increment' => 1,
            ),
          ),
          'settings' => array(
            'default_value' => $fields_defaults[$field_name],
          ),
        );
        $instance = field_read_instance($instance_config['entity_type'], $instance_config['field_name'], $instance_config['bundle']);

        // If node type is selected & instance doesn't exist, create instance.
        if ($delayable === $machine_name && !$instance) {
          field_create_instance($instance_config);
        }

        // If node type isn't selected & instance exists, delete instance.
        if ($delayable === 0 && $instance) {
          field_delete_instance($instance, FALSE);
        }
      }

      // Fieldgroup configuration.
      $group_exist = field_group_exists(DELAYED_PUBLICATION_FIELD_SET . $machine_name, 'node', $machine_name, 'form');
      $group = new stdClass();
      $group->identifier = DELAYED_PUBLICATION_FIELD_SET . $machine_name . '|node|' . $machine_name . '|form';
      $group->group_name = DELAYED_PUBLICATION_FIELD_SET . $machine_name;
      $group->entity_type = 'node';
      $group->bundle = $machine_name;
      $group->mode = 'form';
      $group->disabled = FALSE;
      $group->label = t("Delayed publication");
      $group->weight = '1';
      $group->children = $fields;
      $group->format_type = 'tab';
      $group->format_settings = array(
        'formatter' => 'closed',
        'instance_settings' => array(
          'description' => '',
          'classes' => '',
          'required_fields' => 1,
        ),
      );

      // If node type is selected & fieldgroup doesn't exist, create fieldgroup.
      if ($delayable === $machine_name && !$group_exist) {
        field_group_group_save($group);
      }

      // If node type isn't selected & fieldgroup exists, delete fieldgroup.
      if ($delayable === 0 && $group_exist) {
        field_group_group_export_delete($group);
      }
    }

    // Drupal variable setting.
    variable_set('delayed_publication_node_types', $form_state['values']['delayed_publication_admin_delayable_node_types']);

    // Confirmation.
    drupal_set_message(t("Configuration saved"), 'status');
  }
}
