<?php

/**
 * @file
 * CRON functions.
 *
 * @author Philippe MOUCHEL.
 */

/**
 * Get nids of unpublished nodes from authorized types.
 *
 * @return array
 *   containing unpublished nids
 */
function _delayed_publication_get_unpublished_nids() {
  $nids_to_publish = array();

  // Get concerned node types.
  $node_types = array();
  $types = variable_get('delayed_publication_node_types', array());
  foreach ($types as $machine_name => $delayable) {
    if ($machine_name === $delayable) {
      $node_types[] = $machine_name;
    }
  }
  if (empty($node_types)) {
    return FALSE;
  }

  // Get unpublished nids from concerned node types.
  $efq = new EntityFieldQuery();
  $efq->entityCondition('entity_type', 'node')
      ->propertyCondition('type', $node_types)
      ->propertyCondition('status', 0);
  $result = $efq->execute();
  if (!empty($result) && isset($result['node'])) {
    foreach ($result['node'] as $nid => $tiny_node) {
      $nids_to_publish[] = $tiny_node->nid;
    }
  }

  // Return nids.
  return $nids_to_publish;
}

/**
 * Publish a node if its delayed publication date is set and exceeded.
 *
 * @param string $nid
 *   containing nid to process
 */
function _delayed_publication_autopublish_node($nid = NULL) {
  $now = time();

  // Load node.
  $node = node_load($nid);
  if ($node && isset($node->{DELAYED_PUBLICATION_FIELD_NAME}) && isset($node->{DELAYED_PUBLICATION_FIELD_NAME}[LANGUAGE_NONE][0])) {

    // Check if node has to be published.
    if ($node->status == 0 && $node->{DELAYED_PUBLICATION_FIELD_NAME}[LANGUAGE_NONE][0]['value'] <= $now) {

      // Status update (publication).
      $node->status = 1;

      // Set new real published date.
      $node->{DELAYED_PUBLICATION_REAL_PUBLISHED_FIELD_NAME}[LANGUAGE_NONE][0]['value'] = $node->{DELAYED_PUBLICATION_FIELD_NAME}[LANGUAGE_NONE][0]['value'];

      // Unset delayed publication date.
      $node->{DELAYED_PUBLICATION_FIELD_NAME}[LANGUAGE_NONE][0]['value'] = NULL;

      // Save.
      node_save($node);
    }
  }
}
