
-- SUMMARY --

This module add a field indicating the desired publication date.
Publication of the node will be delayed until this date.

This module add also a real published date field, because $node->created may be
different from real published date.

For a full description of the module, visit the project page:
  http://drupal.org/sandbox/OwilliwO/1858190 (sandbox)

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/node/add/project-issue/1858190 (sandbox)


-- REQUIREMENTS --

* date module
* field_group module
* views_bulk_operations module


-- CONFIGURATION --

* Configure user permissions :
  - Administration » People » Permissions

* Select node types :
  - Administration » Configuration » Delayed publication

* Process bulk update :
  - Administration » Configuration » Delayed publication » Bulk update

* List awaiting publication nodes :
  - Administration » Configuration » Delayed publication » Awaiting publication


-- CONTACT --

Current maintainer:
* Philippe MOUCHEL (fr) - http://drupal.org/user/2393662
