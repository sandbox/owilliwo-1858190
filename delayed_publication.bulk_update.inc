<?php

/**
 * @file
 * Bulk update functions.
 *
 * @author Philippe MOUCHEL.
 */

/**
 * Bulk update page callback.
 *
 * @return string
 *   containing HTML page content
 */
function _delayed_publication_bulk_update_page() {

  // Count nodes which need update.
  $nodes_to_update = _delayed_publication_get_nodes_to_update();
  if (!$nodes_to_update) {
    $nb_nodes_to_update = 0;
  }
  else {
    $nb_nodes_to_update = count($nodes_to_update);
  }

  // Display settings.
  if ($nb_nodes_to_update == 0) {
    $catchline = t('<b>0 nodes</b> must be updated.');
  }
  elseif ($nb_nodes_to_update == 1) {
    $catchline = t('<b>1 node</b> must be updated. <em>@field_name</em> will be set with <em>$node->created</em> value.', array('@field_name' => DELAYED_PUBLICATION_REAL_PUBLISHED_FIELD_NAME));
  }
  else {
    $catchline = t('<b>@nb nodes</b> must be updated. <em>@field_name</em> will be set with <em>$node->created</em> value.', array('@nb' => $nb_nodes_to_update, '@field_name' => DELAYED_PUBLICATION_REAL_PUBLISHED_FIELD_NAME));
  }

  // Form to launch bulk update.
  $bulk_update_form = NULL;
  if ($nb_nodes_to_update > 0) {
    $bulk_update_form = drupal_get_form('_delayed_publication_bulk_update_form');
  }

  // Theme calling.
  $theme = array(
    'thid' => 'bulk-update-page',
    'vars' => array(
      'catchline' => $catchline,
      'bulk_update_form' => drupal_render($bulk_update_form),
    ),
  );
  return theme($theme['thid'], $theme['vars']);
}

/**
 * Bulk update form settings.
 *
 * @return array
 *   containing form settings
 */
function _delayed_publication_bulk_update_form() {
  $form = array();

  // Submit button.
  $form['delayed_publication_bulk_op_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
  );

  // Create and display form.
  return $form;
}

/**
 * Bulk update form submission.
 *
 * @param array $form
 *   containing form settings
 * @param array $form_state
 *   containing form values
 */
function _delayed_publication_bulk_update_form_submit($form, &$form_state) {

  // Get nids to process.
  $operations = array();
  $nodes_to_update = _delayed_publication_get_nodes_to_update();
  $eclated_nodes_to_update = array_chunk($nodes_to_update, 5);
  foreach ($eclated_nodes_to_update as $op_number => $five_nodes) {
    $operations[] = array(
      '_delayed_publication_bulk_update_batch_work',
      array(
        $five_nodes,
        $op_number + 1,
      ),
    );
  }

  // Batch initialization.
  $batch = array(
    'title' => t('Bulk updating nodes'),
    'operations' => $operations,
    'finished' => '_delayed_publication_bulk_update_batch_finished',
    'file' => drupal_get_path('module', 'delayed_publication') . '/delayed_publication.bulk_update.inc',
  );

  // Batch processing.
  batch_set($batch);
}

/**
 * Batch action for each nids.
 *
 * @param array $nids
 *   containing nids to update
 * @param int $op_number
 *   containing operation number
 * @param array $context
 *   containing context informations
 */
function _delayed_publication_bulk_update_batch_work($nids, $op_number, &$context) {
  foreach ($nids as $nid) {

    // Load, update and save node.
    $node = node_load($nid);
    $node->{DELAYED_PUBLICATION_REAL_PUBLISHED_FIELD_NAME}[LANGUAGE_NONE][0]['value'] = $node->created;
    node_save($node);

    // Context display setting.
    $context['results'][] = array(
      'nid' => $node->nid,
      'title' => check_plain($node->title),
      'time' => time(),
    );
  }

  // Display under progress bar.
  $tr = array(
    '@opn' => $op_number,
    '@upns' => implode(', ', $nids),
  );
  $context['message'] = t('Operation n°@opn : processing nodes @upns', $tr);
}

/**
 * Batch finished.
 *
 * @param bool $success
 *   containing batch success
 * @param array $results
 *   containing operations results
 * @param array $operations
 *   containing processed operations
 */
function _delayed_publication_bulk_update_batch_finished($success, $results, $operations) {

  // Display success information.
  if ($success) {
    $first_updated_node = current($results);
    $last_updated_node = end($results);
    $final_result = $last_updated_node['title'] . ' (' . $last_updated_node['nid'] . ')';

    // Count updated nodes.
    $tr = array('@count' => count($results));
    drupal_set_message(t('@count nodes were updated.', $tr));

    // Informations about last updated node.
    $tr = array('@final_result' => $final_result);
    drupal_set_message(t('Last updated node was <em>@final_result</em>', $tr));

    // Informations about execution time.
    $time = $last_updated_node['time'] - $first_updated_node['time'];
    $tr = array('@time' => $time);
    drupal_set_message(t('Processing time was <em>@time seconds</em>', $tr));
  }

  // Else, error reporting.
  else {
    $error_operation = reset($operations);
    $printed_error = print_r($error_operation[0], TRUE);
    $tr = array('@op' => $error_operation[0], '@args' => $printed_error);
    drupal_set_message(t('An error occurred while processing @op with arguments : @args', $tr));
  }
}

/**
 * Get nodes which have to be updated.
 *
 * @return array
 *   or FALSE if empty
 */
function _delayed_publication_get_nodes_to_update() {

  // Get concerned node types (or FALSE).
  $delayed_publication_node_types = variable_get('delayed_publication_node_types', array());
  $node_types = array();
  foreach ($delayed_publication_node_types as $machine_name) {
    if ($machine_name !== 0) {
      $node_types[] = $machine_name;
    }
  }
  if (empty($node_types)) {
    return FALSE;
  }

  // Get nids.
  // 2 query, because one isn't possible.
  // 1. Get nodes which real published date is set.
  $efq = new EntityFieldQuery();
  $efq->entityCondition('entity_type', 'node')
      ->propertyCondition('type', $node_types)
      ->propertyCondition('status', '1')
      ->fieldCondition(DELAYED_PUBLICATION_REAL_PUBLISHED_FIELD_NAME, 'value', NULL, 'IS NOT NULL');
  $result = $efq->execute();

  // 2. Get other nodes by excluding nodes which real published date is set.
  $nodes_with_date = array();
  if (!empty($result) && isset($result['node'])) {
    foreach ($result['node'] as $nid => $tiny_node) {
      $nodes_with_date[] = $tiny_node->nid;
    }
  }

  $efq = new EntityFieldQuery();
  $efq->entityCondition('entity_type', 'node')
      ->propertyCondition('type', $node_types)
      ->propertyCondition('status', '1');
  if (!empty($nodes_with_date)) {
    $efq->propertyCondition('nid', $nodes_with_date, 'NOT IN');
  }
  $result = $efq->execute();

  // Return only nids.
  $nodes_without_date = array();
  if (!empty($result) && isset($result['node'])) {
    foreach ($result['node'] as $nid => $tiny_node) {
      $nodes_without_date[] = $tiny_node->nid;
    }
  }
  return $nodes_without_date;
}
